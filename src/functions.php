<?php

use GuzzleHttp\Psr7\Request;

function parseToken($token) {
    return json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $token)[1]))));
}

function validateToken($token) {
    global $logger;

    $clientId=$_ENV['SSO_CLIENT_ID'];
    $clientSecret=$_ENV['SSO_CLIENT_SECRET'];
    $client = new \GuzzleHttp\Client(
        [
            'base_uri' => $_ENV['SSO_BASE_URL'],
        ]
    );

    try {
        $response = $client->post(
            'protocol/openid-connect/token/introspect',
            [
                'form_params' => [
                    'client_secret' => $clientSecret,
                    'client_id' => $clientId,
                    'token' => $token
                ]
            ]);
    } catch (GuzzleException $exception) {
        $logger->info('error = ' . var_export($exception,true));
        return false;
    }

    return (string) $response->getBody();
}


function saveJob($data, $token) {
    global $logger;
    $validatedToken = validateToken($token);
    //ToDo
    $logger->info('got token and data. ToDo: save job');
    return true;
}