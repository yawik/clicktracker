<?php
// simple echo server. For testing and debugging

chdir(__DIR__);
if (file_exists(__DIR__.'/../vendor/autoload.php')) {
    $loader = include __DIR__.'/../vendor/autoload.php';
} else {
    die("run composer install");
}

include "functions.php";

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$logger = new Logger('collect');
$logger->pushHandler(new StreamHandler(__DIR__.'/../var/log/collect.log', Logger::DEBUG));

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('HTTP/1.0 204 No Content');
    header('Allow: POST');
    header('Access-Control-Allow-Origin: ' . $_ENV['ACCESS_CONTROL_ALLOW_ORIGIN']);
    header('Access-Control-Allow-Methods: ' . $_ENV['ACCESS_CONTROL_ALLOW_METHODS']);
    header('Access-Control-Allow-Headers: ' . $_ENV['ACCESS_CONTROL_ALLOW_HEADERS']);
    header('Content-Type: ', true);
    $logger->info('Request Method: Options');
    exit;
}

$content_type = $_SERVER['CONTENT_TYPE'];

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('HTTP/1.0 405 Method Not Allowed');
    header('Allow: POST');
    $logger->info('Request Method: != POST');
    exit;
}

// Check origin
$origin = $_SERVER['HTTP_ORIGIN'] ?? '';
#if (!preg_match('~^https://form.yawik.org~', $origin)) {
#    header('HTTP/1.0 403 Forbidden');
#    header('X-Server-Origin: ' . $_SERVER['HTTP_ORIGIN']);
#    exit;
#}

header('Access-Control-Allow-Origin: ' . $origin);
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
header('Vary: Origin');

$raw = file_get_contents('php://input');

// if you send a json in Postman as raw data, get.
if (count($_POST) == 0) {
    if (is_null($data = json_decode($raw, true))) {
        $logger->error('raw = ' . $raw);
    };
} elseif(count($_POST) == 1 && current($_POST) == "") {
    $raw = stripslashes($raw);
    if(is_null($data = json_decode($raw, true))) {
        $logger->error('raw = ' . $raw);
    }
} else {
    $data = $_POST;
    $logger->info('count(_POST) = ' . count($_POST));
    $logger->info('key(_POST) = ' . key($_POST));
    $logger->info('current(_POST) = ' . current($_POST));
}

switch(json_last_error()) {
    case JSON_ERROR_NONE:
        $logger->debug('Json Decoded');
    break;
    case JSON_ERROR_DEPTH:
        $logger->error(' - Maximale Stacktiefe überschritten');
    break;
    case JSON_ERROR_STATE_MISMATCH:
        $logger->error(' - Unterlauf oder Nichtübereinstimmung der Modi');
    break;
    case JSON_ERROR_CTRL_CHAR:
        $logger->error(' - Unerwartetes Steuerzeichen gefunden');
    break;
    case JSON_ERROR_SYNTAX:
        $logger->error(' - Syntaxfehler, ungültiges JSON');
    break;
    case JSON_ERROR_UTF8:
        $logger->error(' - falsche UTF-8 Zeichen, möglicherweise fehlerhaft kodiert');
    break;
}

// Filtering
/**
$config = \HTMLPurifier_Config::createDefault();
$purifier = new \HTMLPurifier($config);
$content = $purifier->purify($content);
 **/

if (isset($_SERVER['HTTP_X_ACCESS_TOKEN'])) {
    $token=parseToken($_SERVER['HTTP_X_ACCESS_TOKEN']);
    $validatedToken=validateToken($_SERVER['HTTP_X_ACCESS_TOKEN']);
    saveJob($data, $_SERVER['HTTP_X_ACCESS_TOKEN']);
}

$response = [
    'ok' => true,
    'data' => $data,
];

$logger->info(var_export($response, true));

header('Content-Type: application/json');
echo json_encode($response);