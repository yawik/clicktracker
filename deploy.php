<?php
namespace Deployer;

require 'recipe/zend_framework.php';

// Project name
set('application', 'API');

// Project repository
set('repository', 'git@gitlab.com:yawik/clicktracker.git');

// Shared files/dirs between deploys
add('shared_files', [
    'public/.htaccess',
    '.env.local'
]);
add('shared_dirs', [
    'var/log',
    'config/autoload',
    'public/static'
]);

// Writable dirs by web server
add('writable_dirs', [
    'var/cache',
    'var/log',
    'public/static'
]);

set('default_stage', 'prod');
//set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader');


host('api.yawik.org')
    ->user('yawik')
    ->stage('prod')
    ->multiplexing(false)
    ->set('deploy_path', '/var/www/collector');

#after('deploy:symlink', 'cachetool:clear:opcache');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

