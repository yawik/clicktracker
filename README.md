# ClickTracker

not only for job advertisements is it necessary to determine some key figures that reflect the success of a 
job advertisement even before an application is received. 

These include:

- How often was a job ad called up.
- How many people called up the job ad,
- Where was a job ad called up.

At the same time, there are requirements

- data protection must be guaranteed. It must not be possible to create user profiles.
- No JS frameworks. Vanilla JS only.

A rough plan foresees:

- Differentiation between pageview and reload using UUID. If there is a UUID for the current URL, increment a reload counter. If there is no UUID yet, create a UUID and increment a pageview counter.
- Store the time in LocalStorage and start a timer.
- After an event that signals that the user has left the page, send pageview, reload, time, duration to an endpoint.

## License

MIT

## Project status

Planning
